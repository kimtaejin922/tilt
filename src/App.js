import React, { useState, useEffect } from 'react';
import "./App.css";
import Ball from "./ball.png";

const g10ms = 0.00098;

function App() {
  const [stage, setStage] = useState(0);
  const [state, setState] = useState({
    maze: [
      [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0],
        [0, 1, 2, 1, 0, 0, 0, 0, 3, 3, 3, 0],
      ],
      [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 1, 2, 1, 0, 3, 3, 0],
      ],
      [
        [0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 0],
      ],
      [
        [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 3, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      ]
    ],
    isGameOver: false,
    isSuccess: false,
    ball: null,
    beta: 0,
    gamma: 0,
    vx: 0,
    vy: 0,
    coefficient: 200,
    hx: 0,
    hy: 0
  });

  useEffect(()=>{
    retry();
  }, [stage]);

  useEffect(() => {
    setBall();

    setInterval(() => {
      state.vx += g10ms * Math.sin(state.gamma / 90);
      state.hx += state.isGameOver ? state.vx * 30 : state.vx * state.coefficient;

      state.vy += g10ms * Math.sin(state.beta / 90);
      state.hy += state.isGameOver ? state.vy * 30 : state.vy * state.coefficient;

      let mapSize = Math.min(window.innerWidth, window.innerHeight);
      const tableElement = document.getElementsByClassName("board")[0];
      let tableRect = tableElement.getBoundingClientRect();

      if (state.hx > tableRect.right - 30) {
        state.hx = tableRect.right - 30;
        state.vx = 0;
      }
      if (state.hx < tableRect.left) {
        state.hx = 0;
        state.vx = 0;
      }
      if (state.hy < tableRect.top) {
        state.hy = tableRect.top;
        state.vy = 0;
      }
      if (state.hy > tableRect.bottom - 30) {
        state.hy = tableRect.bottom - 30;
        state.vy = 0;
      }

      // move the ball image
      if (!state.isSuccess) {
        state.ball.style.left = state.hx + "px";
        state.ball.style.top = state.hy + "px";
      }

      // define game over
      if (!state.isGameOver && !state.isSuccess && !document.elementFromPoint(state.hx + 15, state.hy + 15).classList.contains("safe")) {
        document.getElementById("gameover").style.display = "block";
        document.getElementById("overray").style.display = "block";
        state.isGameOver = true;
        playBallFallingEffect();
      }

      // define success
      if (!state.isGameOver && !state.isSuccess && document.elementFromPoint(state.hx, state.hy).classList.contains("finish")) {
        // state.isGameOver = true;
        state.isSuccess = true;
        document.getElementById("success").style.display = "block";
        document.getElementById("overray").style.display = "block";
      }
    }, 10);

    window.addEventListener("deviceorientation", handleOrientation, true);
    function handleOrientation(event) {
      state.beta = event.beta;
      state.gamma = event.gamma;
    }

  }, []);

  function retry() {
    state.isGameOver = false;
    state.isSuccess = false;
    document.getElementById("success").style.display = "none";
    document.getElementById("gameover").style.display = "none";
    document.getElementById("overray").style.display = "none";

    state.vx = 0;
    state.vy = 0;

    setBall();
  }

  function setBall() {
    state.ball = document.getElementById("ball");

    state.ball.width = 30;
    state.ball.height = 30;

    let start = document.getElementById("start");
    let rect = start.getBoundingClientRect();
    state.hx = rect.left;
    state.hy = rect.top;
    state.beta = 0;
    state.gamma = 0;

    state.vx = 0;
    state.vy = 0;
    state.ball.style.left = state.hx + "px";
    state.ball.style.top = state.hy + "px";
  }

  async function playBallFallingEffect() {
    for (let i = 0; i < 30; i++) {
      if (state.isGameOver) {
        await reduceBallSize();
      }
    }
  }

  async function reduceBallSize() {
    return new Promise(resolve => {
      setTimeout(() => {
        if (state.ball.width > 0) {
          state.ball.style.left = `${Number(state.ball.style.left.slice(0, -2)) + 0.5}px`;
          state.ball.style.top = `${Number(state.ball.style.top.slice(0, -2)) + 0.5}px`;
          state.ball.width = state.ball.width - 1;
          state.ball.height = state.ball.height - 1;
        }
        resolve();
      }, 50);
    })
  }

  return (
    <div className="board-wrapper">
      <table className="board">
        <tbody>
          {state.maze[stage].map((row, rIndex) =>
            <tr key={rIndex}>
              {row.map((col, jIndex) => {
                if (col === 2) return <td key={jIndex} className='safe' id="start"></td>
                if (col === 3) return <td key={jIndex} className='safe finish'></td>
                return <td key={jIndex} className={col && 'safe'}></td>
              })}
            </tr>
          )}
        </tbody>
      </table>
      <img id="ball" src={Ball} alt="ball" width="50" height="50" />
      <div id="overray">
        <div id="gameover">
          <p>Game Over</p>
          <button id="retry" onClick={retry}>RETRY</button>
        </div>
        <div id="success">
          <p>Sucess!</p>
          {stage + 1 < state.maze.length && <button id="retry" onClick={() => {setStage(stage+1)}}>NEXT STAGE</button>}
        </div>
      </div>
    </div>
  );
}

export default App;
